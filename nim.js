// global constants
const c = String.fromCharCode(0x07f7); // O=79 127 0x01b1 0x047c 0x05d0
const cc = String.fromCharCode(79);
let players = ["You", "Computer"];

function computerPlays(arows, leaveone) {
  // version 2019 02 15 -- loop condition bug fixed
  // params arows array of row counts
  // leaveone true if winner leaves last one for the loser
  // returns [delta,hasit,true/false] expected delta<0, hasit>-1, has sol or not
  // delta is how many tokens, hasit is row index where taken
  var apow = [],
    ax = [],
    ay = [];
  var numbits = Math.floor(Math.log2(Math.max(...arows))) + 1;
  var numrows = arows.length;
  for (var i = 0; i < numbits; i++) {
    apow.push(Math.pow(2, i));
  }
  console.info("**rows: " + arows + " leave one rule: " + leaveone);
  for (var j = 0; j < numrows; j++) {
    ax[j] = [];
  }
  // build ax=2d array of pow_of_2 decomp values of #of tokens each row
  for (var j = 0; j < numrows; j++) {
    for (var i = 0; i < numbits; i++) {
      if (apow[i] & arows[j]) {
        ax[j][i] = 1;
      } else ax[j][i] = 0;
    }
  }
  // build ay=parity index of each pow_of_2 on board
  for (var i = 0; i < numbits; i++) {
    ay[i] = 0;
    for (var j = 0; j < numrows; j++) {
      ay[i] = (ay[i] + ax[j][i]) % 2;
    }
  }
  // biggest odd pow_of_2
  for (var i = numbits - 1; i >= 0; i--) {
    if (ay[i] > 0) {
      break;
    }
  }

  var imax = i,
    delta = 0,
    hasit = -1;
  //if index hasit stays -1 then no solution :(
  if (imax >= 0) {
    for (var j = 0; j < numrows; j++) {
      if (ax[j][imax] > 0) {
        hasit = j;
        break;
      }
    }

    for (var i = 0; i < numbits; i++) {
      if (ay[i] > 0) {
        if (ax[hasit][i] == 0) {
          delta += apow[i];
        } else {
          delta -= apow[i];
        }
      }
    }
    //console.info("* ay: " + ay + " imax " + imax + " hasit " + hasit + " delta " + delta);

    var newvalue = arows[hasit] + delta;
    // leave one rule - check not even number of singles
    if (leaveone && newvalue < 2) {
      var anyabove = false;
      for (var j = 0; j < numrows; j++) {
        if (j !== hasit && arows[j] > 1) {
          anyabove = true;
          console.info("anyabove: true");
        }
      }
      if (!anyabove) {
        newvalue = (newvalue + 1) % 2;
        delta = newvalue - arows[hasit];
        console.info("needed adjust for leave one rule");
      }
    }
    //console.info('select row: ' + hasit + ' remove: ' + (-delta) + '   replace ' + arows[hasit] + ' with ' + newvalue);
  } else {
    console.info("my opponent has a winning position :( ");
    var hasone = -1;
    for (var j = 0; j < numrows; j++) {
      if (arows[j] > 0) {
        hasone = j;
        break;
      }
    }
    return [-1, hasone, false];
  }
  return [delta, hasit, true];
}
// ***************************************************************************************
function Coin(props) {
  return (
    <button
      className={props.value === cc ? "btn red" : "btn"}
      disabled={props.disabled}
      onClick={props.onClick}
    >
      {props.value}
    </button>
  );
}

function Done(props) {
  return (
    <div className={props.display}>
      <button className="done" onClick={props.onClick}>
        OK
      </button>
    </div>
  );
}

function Reset(props) {
  return (
    <div className={props.display}>
      <button className="done" onClick={props.onClick}>
        Reset Game
      </button>
    </div>
  );
}

function GameOver(props) {
  return (
    <h1 className={props.display}>
      Winner: {props.player1 ? players[1] : props.name1}
    </h1>
  );
}

function Counter(props) {
  let cnt = 0; //
  let player = props.player1 ? props.name1 : players[1];
  props.myboard.map(r => r.map(v => (cnt = cnt + (v === "" ? 0 : 1))));
  return (
    <div>
      Player: <b>{player}</b>- Tokens remaining: <b>{cnt}</b>
    </div>
  );
}

class Name1Input extends React.Component {
  constructor(props) {
    super(props);
  }
  handleChange = e => {
    this.props.onName1Change(e.target.value);
  };
  render() {
    const name1 = this.props.name1;
    return (
      <label>
        <legend>Player 1 Name: </legend>
        <input value={name1} onChange={this.handleChange} />
        <br />
      </label>
    );
  }
}

class LevelSelect extends React.Component {
  constructor(props) {
    super(props);
  }
  handleChange = e => {
    this.props.onLevelChange(e.target.value);
  };
  render() {
    const level = this.props.level;
    return (
      <label>
        Pick your level:
        <select value={this.props.level} onChange={this.handleChange}>
          <option value="easy">Easy</option>
          <option value="medium">Medium</option>
          <option value="hard">Hard</option>
          <option value="very hard">Very Hard</option>
        </select>
      </label>
    );
  }
}

class ComputerMove extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidUpdate(prevProps, prevState) {
    if (!this.props.gameOver && !this.props.player1 && prevProps.player1) {
      console.log("PLAYING =");
      this.props.computerplays(); // calls c p  aft delay
    }
  }
  render() {
    return <h5 className={this.props.display}>Computer's turn...</h5>;
  }
}

class ComputerDisplay extends React.Component {
  constructor(props) {
    super(props);
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.display == "block" &&
      !this.props.gameOver &&
      !this.props.player1
    ) {
      //
      this.props.computerdisplays(); // calls c p  aft delay
    }
  }
  render() {
    return (
      <span className={this.props.display}>... removing red tokens ...</span>
    );
  }
}

class Board extends React.Component {
  constructor(props) {
    super(props);
    let layout = [3, 3, 3];
    let myboard = [];
    layout.map(i => {
      let myrow = Array(i).fill(c);
      myboard = [...myboard, myrow];
    });
    this.state = {
      myboard: myboard,
      rowSelected: -1,
      player1: true,
      gameOver: false,
      name1: "You",
      level: "easy",
      computerHasPlayed: false,
      computerHasDisplayed: false
    };
  }

  computerPlay = () => {
    if (this.state.gameOver) {
      this.setState({
        player1: true
      });
    } else {
      let myboard = this.state.myboard.slice();
      let rowcnts = [];
      myboard.map((r, i) => rowcnts.push(this.rowCount(i)));
      console.log(rowcnts);
      let [delta, hasit, success] = computerPlays(rowcnts, true);
      console.log(delta, hasit, success);
      let i = 0;
      do {
        i++;
        let idx = myboard[hasit].indexOf(c);
        myboard[hasit][idx] = cc;
      } while (i < -delta);
      const player1 = false,
        computerHasDisplayed = false,
        computerHasPlayed = true;
      let cnt = 0;
      myboard.map(r => r.map(v => (cnt = cnt + (v === "" ? 0 : 1))));
      const gameOver = cnt === 1;
      this.setState(state => ({
        player1,
        myboard,
        computerHasPlayed,
        computerHasDisplayed,
        gameOver
      }));
    }
  };

  computerDisplay = () => {
    if (this.state.gameOver) {
      this.setState({
        player1: true
      });
    } else {
      let myboard = this.state.myboard.slice();
      /* */
      let rowcnts = [];
      myboard.map((r, i) => rowcnts.push(this.rowCount(i)));
      console.log(rowcnts);
      let [delta, hasit, success] = computerPlays(rowcnts, true);
      console.log(delta, hasit, success);
      /*  */
      let i = 0;
      do {
        i++;
        let idx = myboard[hasit].indexOf(cc);
        myboard[hasit][idx] = "";
      } while (i < -delta);
      const player1 = true,
        computerHasPlayed = false,
        computerHasDisplayed = true;
      let cnt = 0;
      myboard.map(r => r.map(v => (cnt = cnt + (v === "" ? 0 : 1))));
      const gameOver = cnt === 1;
      this.setState(state => ({
        player1,
        myboard,
        computerHasPlayed,
        computerHasDisplayed,
        gameOver
      }));
    }
  };

  computerDisplayAfterDelay = () => {
    setTimeout(() => this.computerDisplay(), 3000);
  };

  computerPlayAfterDelay = () => {
    setTimeout(() => this.computerPlay(), 1500);
  };

  handleLevelChange = level => {
    let layout = [];
    switch (level) {
      case "easy":
        layout = [3, 3, 3];
        break;
      case "medium":
        layout = [1, 2, 3, 4, 5];
        break;
      case "hard":
        layout = [1, 2, 3, 4, 5, 6];
        break;
      case "very hard":
        layout = [1, 2, 3, 4, 5, 6, 7];
        break;
    }
    let myboard = [];
    layout.map(i => {
      let myrow = Array(i).fill(c);
      myboard = [...myboard, myrow];
    });
    const gameOver = false,
      player1 = true,
      rowSelected = -1;
    //console.log("level ", level, layout);
    this.setState({ myboard, level, gameOver, player1, rowSelected });
  };

  rowCount(rowSelected) {
    if (rowSelected < 0) return -1;
    let row = this.state.myboard[rowSelected].slice();
    let cnt = 0;
    row.map(v => (cnt = cnt + (v === "" ? 0 : 1)));
    return cnt;
  }

  //handleName1Change(name1) {//
  handleName1Change = name1 => {
    // alternate syntax to avoid bind
    this.setState({ name1 });
  };

  handleCoinClick(i, j) {
    let myboard = this.state.myboard.slice(); //  copy
    let cnt = 0;
    myboard.map(r => r.map(v => (cnt = cnt + (v === "" ? 0 : 1))));
    let rowSelected = this.state.rowSelected == -1 ? i : this.state.rowSelected;
    myboard[i][j] = "";
    let rowCount = this.rowCount(rowSelected);
    let player1 = this.state.player1;
    if (rowCount === 0) {
      // switch players
      rowSelected = -1;
      player1 = !player1;
    }
    // Check if winner with this click:
    const gameOver = cnt <= 2 && rowSelected == -1;
    this.setState({ myboard, rowSelected, player1, gameOver });
  } //

  handleResetClick() {
    console.log("reset requested to " + this.state.level + " level.");
    this.handleLevelChange(this.state.level);
    let player1 = true;
  }

  handleDoneClick() {
    let rowSelected = -1;
    let player1 = !this.state.player1;
    let cnt = 0;
    this.state.myboard.map(r => r.map(v => (cnt = cnt + (v === "" ? 0 : 1))));
    const gameOver = cnt === 1;
    this.setState({ player1, rowSelected, gameOver });
  }

  render() {
    return (
      <React.Fragment>
        <h3>Nim - Single file React - readable JSX </h3>
        <Name1Input
          name1={this.state.name1}
          onName1Change={this.handleName1Change}
        />
        <LevelSelect
          level={this.state.level}
          onLevelChange={this.handleLevelChange}
        />
        <hr />
        <Counter
          player1={this.state.player1}
          name1={this.state.name1}
          myboard={this.state.myboard}
        />
        {this.state.myboard.map((r, i) => {
          return (
            <div key={i}>
              {r.map((v, j) => {
                let disbool = false;
                return (
                  <Coin
                    key={j}
                    value={v}
                    disabled={
                      v.length
                        ? (!this.state.gameOver &&
                            this.state.player1 &&
                            this.state.rowSelected < 0) ||
                          this.state.rowSelected == i
                          ? ""
                          : "disabled"
                        : "disabled"
                    }
                    onClick={() => this.handleCoinClick(i, j)}
                  />
                );
              })}
            </div>
          );
        })}
        <Done
          display={this.state.rowSelected == -1 ? "none" : "block"}
          onClick={() => this.handleDoneClick()}
        />
        <ComputerMove
          display={this.state.player1 || this.state.gameOver ? "none" : "block"}
          gameOver={this.state.gameOver}
          player1={this.state.player1}
          computerplays={() => this.computerPlayAfterDelay()}
        />
        <ComputerDisplay
          display={
            !this.state.computerHasPlayed ||
            (this.state.player1 || this.state.gameOver)
              ? "none"
              : "block"
          }
          gameOver={this.state.gameOver}
          player1={this.state.player1}
          computerdisplays={() => this.computerDisplayAfterDelay()}
        />
        <GameOver
          player1={this.state.player1}
          name1={this.state.name1}
          display={this.state.gameOver ? "block" : "none"}
        />
        <br />
        <br />
        <Reset display={"block"} onClick={() => this.handleResetClick()} />
      </React.Fragment>
    );
  }
}

ReactDOM.render(<Board />, document.getElementById("root"));
